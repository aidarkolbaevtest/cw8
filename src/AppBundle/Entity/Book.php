<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookRepository")
 * @Vich\Uploadable
 */
class Book
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1024)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string")
     */
    private $author;


    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="books")
     */
    private $categories;


    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Reader", mappedBy="books")
     */
    private $readers;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="book_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;


    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $expected_return_date = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $fixed_return_date = null;


    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $status = true;
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile($image = null)
    {
        $this->imageFile = $image;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    public function getImageName()
    {
        return $this->imageName;
    }


    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->readers = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }


    public function addCategory($category)
    {
        $this->categories->add($category);

        return $this;
    }


    public function getCategories()
    {
        return $this->categories;
    }


    public function addReader($reader)
    {
        $this->readers->add($reader);
        $reader->addBook($this);

        return $this;
    }

    public function getReaders()
    {
        return $this->readers;
    }

    /**
     * @param \DateTime $fixed_return_date
     * @return Book
     */
    public function setFixedReturnDate(\DateTime $fixed_return_date)
    {
        $this->fixed_return_date = $fixed_return_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getFixedReturnDateString()
    {
        return date_format($this->fixed_return_date, 'Y-m-d');
    }

    /**
     * @param string $expected_return_date
     * @return Book
     */
    public function setExpectedReturnDate(string $expected_return_date)
    {
        $this->expected_return_date = date_create_from_format('Y-m-d', $expected_return_date);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpectedReturnDateString()
    {
        return date_format($this->expected_return_date, 'Y-m-d');
    }

    /**
     * @param bool $status
     * @return Book
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStock()
    {
        return $this->status;
    }
}

