<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=512)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=1024)
     */
    private $address;


    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="string", length=1024, unique=true)
     */
    private $passport;


    /**
     * @var string
     *
     * @ORM\Column(name="library_card", type="string", length=1024)
     */
    private $library_card;


    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Book", inversedBy="readers")
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname
     *
     * @param string $fullName
     *
     * @return Reader
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get full_name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $passport
     * @return Reader
     */
    public function setPassport(string $passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param string $library_card
     * @return Reader
     */
    public function setLibraryCard(string $library_card)
    {
        $this->library_card = $library_card;
        return $this;
    }

    /**
     * @return string
     */
    public function getLibraryCard()
    {
        return $this->library_card;
    }



    public function addBook($book)
    {
        $this->books->add($book);

        return $this;
    }


    public function getBooks()
    {
        return $this->books;
    }
}

