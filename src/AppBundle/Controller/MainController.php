<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $genres = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $books = $this->getDoctrine()->getRepository('AppBundle:Book')->findAll();
        return $this->render('@App/Main/index.html.twig', array(
            'genres' => $genres,
            'books' => $books
        ));
    }


    /**
     * @Route("/category/{id}", requirements={"id" : "\d+"}, name="category")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeCategoryAction($id)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);
        $genres = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $books = $category->getBooks();
        return $this->render('@App/Main/category.html.twig', [
            'books' => $books,
            'genres' => $genres
        ]);
    }


    /**
     * @Route("/registration", name="registration")
     * @param Request $request
     * @return Response
     */
    public function registrationAction(Request $request)
    {
        $reader = new Reader();

        $form_builder = $this->createFormBuilder($reader);
        $form_builder->add('fullName', TextType::class, array('label' => 'ФИО'));
        $form_builder->add('address', TextType::class, array('label' => 'Адрес'));
        $form_builder->add('passport', TextType::class, array('label' => 'ID Пасспорта'));
        $form_builder->add('save', SubmitType::class, array('label' => 'Получить читательсикй билет'));
        $form = $form_builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reader = $form->getData();
            $library_card = md5(date('i-s'));
            $reader->setLibraryCard($library_card);

            $em = $this->getDoctrine()->getManager();
            $em->persist($reader);
            $em->flush();

            return $this->render('@App/Main/success.html.twig', array(
                'card' => $library_card
            ));
        }
        return $this->render('@App/Main/register.html.twig', array(
            'form' => $form->createView()
        ));

    }


    /**
     * @Route("/book/{id}", requirements={"id" : "\d+"}, name="getBook")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function getBookAction(Request $request, $id)
    {
        $book = $this->getDoctrine()->getRepository('AppBundle:Book')->find($id);

        $card_id = $request->get('card_id');
        $date = $request->get('date');
        if (!empty(trim($card_id)) && !empty(trim($date))) {
            $book->setExpectedReturnDate($date);
            $reader = $this->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->getReaderByCard($card_id);
            $book->addReader($reader[0]);
            $book->setStatus(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render('@App/Main/getbook.html.twig', [
            'book' => $book
        ]);
    }


    /**
     * @Route("/my/books", name="my_books")
     * @return Response
     */
    public function myBooksAction()
    {
        return $this->render('@App/Main/mybooks.html.twig', [
            //
        ]);
    }


    /**
     * @Route("/my/books/get", name="get_my_books")
     * @param Request $request
     * @Method("GET")
     * @return JsonResponse
     */
    public function getMyBooksAction(Request $request)
    {
        $reader = $this->getDoctrine()
            ->getRepository('AppBundle:Reader')
            ->getReaderByCard($request->get('card_id'));
        $response_array = [];
        $books = $reader[0]->getBooks();
        foreach ($books as $book) {
            $response_array[] = [
                'name' => $book->getName(),
                'img' => $book->getImageName(),
                'author' => $book->getAuthor()
            ];
        }

        return new JsonResponse([
            'books' => $response_array
        ]);

    }


    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"}, name="change_lang")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeLanguageAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

}
