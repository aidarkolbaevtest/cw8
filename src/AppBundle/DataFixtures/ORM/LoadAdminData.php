<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAdminData extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $admin = new Admin();
        $admin->setUsername('admin');
        $password = password_hash('123', PASSWORD_DEFAULT);
        $admin->setPassword($password);
        $admin->setRole(['ROLE_ADMIN']);

        $manager->flush();

    }

}