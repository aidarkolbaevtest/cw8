<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBooksData extends Fixture implements DependentFixtureInterface
{
    const BOOK1 = '1';
    const BOOK2 = '2';
    const BOOK3 = '3';
    const BOOK4 = '4';
    const BOOK5 = '5';
    const BOOK6 = '6';
    const BOOK7 = '7';
    const BOOK8 = '8';
    const BOOK9 = '9';
    const BOOK10 = '10';
    const BOOK11 = '11';
    const BOOK12 = '12';
    const BOOK13 = '13';
    const BOOK14 = '14';
    public function load(ObjectManager $manager)
    {
        $book_names = [
            'Лолита',
            'Евгений Онегин',
            'Мастер и Маргарита',
            'Война и мир',
            'Анна Каренина',
            'Унесённые ветром',
            'Над пропастью во ржи',
            '1984',
            '451 градус по Фаренгейту',
            'Остров',
            'Возможность острова',
            'Девушка с татуировкой дракона',
            'Код да Винчи',
            'Убийство в «Восточном экспрессе»',
        ];

        $authors = [
            'Владимир Набоков',
            'Александр Пушкин',
            'М. А. Булгаков',
            'Лев Толстой',
            'Лев Толстой',
            'Маргарет Митчелл',
            'Джером Д. Сэлинджер',
            'Джордж Оруэлл',
            'Рэй Бредбери',
            'Олдос Хаксли',
            'Мишель Уэльбек',
            'Стиг Ларссон',
            'Дэн Браун',
            'Агата Кристи'
        ];
        for ($i = 0; $i < count($book_names); $i++) {
            $book = new Book();
            $book->setName($book_names[$i]);
            $book->setAuthor($authors[$i]);
            $book->setImageName('book.png');
            if ($i < 7) {
                $book->addCategory($this->getReference(LoadCategoriesData::ROMAN));
            } elseif ($i < 11) {
                $book->addCategory($this->getReference(LoadCategoriesData::UTOPIA));
                if ($i%2 === 0) {
                    $book->addCategory($this->getReference(LoadCategoriesData::REALISM));
                }
            } else {
                $book->addCategory($this->getReference(LoadCategoriesData::DETECTIVE));
            }
            $manager->persist($book);
        }


        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadCategoriesData::class,
        );
    }

}