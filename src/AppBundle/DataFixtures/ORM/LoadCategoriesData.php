<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoriesData extends Fixture
{
    const ROMAN = 'Роман';
    const UTOPIA = 'Утопия';
    const REALISM = 'Реализм';
    const DETECTIVE = 'Детектив';
    public function load(ObjectManager $manager)
    {
        $categories = [
            'Роман',
            'Утопия',
            'Реализм',
            'Детектив'
        ];
        for ($i = 0; $i < count($categories); $i++) {
            $category = new Category();
            $category->setName($categories[$i]);
            $manager->persist($category);
            switch ($i) {
                case 0:
                    $this->addReference(self::ROMAN, $category);
                    break;
                case 1:
                    $this->addReference(self::UTOPIA, $category);
                    break;
                case 2:
                    $this->addReference(self::REALISM, $category);
                    break;
                case 3:
                    $this->addReference(self::DETECTIVE, $category);
                    break;
            }
        }

        $manager->flush();

    }

}